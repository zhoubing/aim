package de.sbarth.aim.ui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;

import de.sbarth.aim.model.ImageItem;
import de.sbarth.aim.viewmodel.IconOrganisatorViewModel;
import impl.org.controlsfx.skin.GridViewSkin;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

/**
 * TODO:
 * - DPI Strings as constants
 * - add mvvmfx logic
 */

public class IconOrganisatorView implements Initializable {
    @FXML
    private AnchorPane settingsContainer;
    @FXML
    private Button addProjectPathButton;
    @FXML
    private Button copyImagesButton;
    @FXML
    private CheckBox copyAllResolutions;
    @FXML
    private ComboBox<String> cbAllowedDpiType;
    @FXML
    private ComboBox<String> cbProjectPaths;
    @FXML
    private GridPane settingViews;
    @FXML
    private GridView<ImageItem> imagesGridView;
    @FXML
    private ImageView settingsIcon;
    @FXML
    private Label progressValue;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField searchTextField;
    @FXML
    private VBox progressContainer;
    @FXML
    private VBox viewContainer;

    // Others
    private MyGridViewSkin gridViewSkin;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        initUi();
    }

    private void initUi() {
        cbAllowedDpiType.setItems(FXCollections.observableArrayList(Arrays.asList("all", "mdpi", "hdpi", "xhdpi", "xxhdpi", "xxxhdpi", "others")));
        cbAllowedDpiType.getSelectionModel().select(0);
        cbAllowedDpiType.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            gridViewSkin.redrawCells();
        });
        cbProjectPaths.setItems(FXCollections.observableArrayList("C:\\\\Users\\\\x1sba\\\\Downloads\\\\material-design-icons-master"));
        copyImagesButton.setTooltip(new Tooltip("Copy Images to destination folder"));

        gridViewSkin = new MyGridViewSkin(imagesGridView);
        imagesGridView.setCellFactory(param -> new ImageViewCell());
        imagesGridView.setHorizontalCellSpacing(20);
        imagesGridView.setVerticalCellSpacing(20);
        imagesGridView.setSkin(gridViewSkin);

        progressBar.progressProperty().addListener((observable, oldValue, newValue) -> {
            final Double value = (Double) newValue;
            if (value == 1) {
                progressValue.setText("Update UI");
            } else {
                progressValue.setText(UiUtils.getFormattedProgressValue(value));
            }
        });

        searchTextField.setFocusTraversable(false);
        searchTextField.setPromptText("Search for... (split criterias with ';')");

        initSettingsContainer();
    }

    private void initSettingsContainer() {
        settingsIcon.setOnMouseEntered(event -> {
            final KeyValue keyValueWidth = new KeyValue(settingsContainer.prefWidthProperty(), 440);
            final KeyValue keyValueHeight = new KeyValue(settingsContainer.prefHeightProperty(), 160);
            final KeyValue keyValueVisibilitySettingViews = new KeyValue(settingViews.opacityProperty(), 1.0);

            KeyFrame keyFrame = new KeyFrame(Duration.millis(100), keyValueWidth, keyValueHeight, keyValueVisibilitySettingViews);
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(keyFrame);
            timeline.play();
        });
        settingsContainer.setOnMouseExited(event -> {
            if (cbAllowedDpiType.isShowing() || cbProjectPaths.isShowing()) { // if comboBox popup is open, don't close view
                return;
            }

            final KeyValue keyValueWidth = new KeyValue(settingsContainer.prefWidthProperty(), 30);
            final KeyValue keyValueHeight = new KeyValue(settingsContainer.prefHeightProperty(), 30);
            final KeyValue keyValueVisibilitySettingViews = new KeyValue(settingViews.opacityProperty(), 0.0);

            KeyFrame keyFrame = new KeyFrame(Duration.millis(100), keyValueWidth, keyValueHeight, keyValueVisibilitySettingViews);
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(keyFrame);
            timeline.play();
        });
        settingViews.managedProperty().bind(Bindings.createBooleanBinding(() -> !(settingViews.getOpacity() < 1.0), settingViews.opacityProperty()));
    }

    public void setViewModel(final IconOrganisatorViewModel viewModel) {
        copyImagesButton.visibleProperty().bind(viewModel.runningProperty().not());
        copyAllResolutions.selectedProperty().bindBidirectional(viewModel.copyAllResolutions());
        imagesGridView.itemsProperty().bind(viewModel.imageItemsProperty());
        progressBar.progressProperty().bind(viewModel.progressProperty());
        progressBar.visibleProperty().bind(viewModel.runningProperty());
        progressContainer.visibleProperty().bind(viewModel.runningProperty());
        progressValue.visibleProperty().bind(viewModel.runningProperty());
        settingsContainer.visibleProperty().bind(viewModel.runningProperty().not());
        viewContainer.visibleProperty().bind(viewModel.runningProperty().not());
        cbProjectPaths.setItems(UiUtils.getProjectPaths());
        cbProjectPaths.getSelectionModel().select(UiUtils.getSelectedProjectPath());
        cbProjectPaths.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && newValue != null) {
                UiUtils.setSelectedProjectPath(newValue);
            }
        });

        viewModel.searchValueProperty().bind(searchTextField.textProperty());
        viewModel.allowedDpiTypeProperty().bind(cbAllowedDpiType.getSelectionModel().selectedItemProperty());

        copyImagesButton.setOnAction(event -> {
            viewModel.copyImages();

            final String notificationMessage = String.format("Images copied successfully to '%s'", UiUtils.getSelectedProjectPath());
            showPopupMessage(notificationMessage, viewContainer, 100, 20);
        });
        addProjectPathButton.setOnAction(event -> {
            viewModel.addProjectPath();
            cbProjectPaths.setItems(UiUtils.getProjectPaths());
            cbProjectPaths.getSelectionModel().select(UiUtils.getSelectedProjectPath());
        });
    }

    public Popup createPopup(final String message) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        final Label label = new Label(message);
        label.setOnMouseReleased(event -> popup.hide());
        label.setMaxWidth(350);
        label.setWrapText(true);

        final Button button = new Button("OK");
        button.getStyleClass().addAll("toolbar-button");
        button.setMaxWidth(Double.MAX_VALUE);
        button.setOnAction(event -> popup.hide());

        final VBox container = new VBox(label, button);
        container.setAlignment(Pos.CENTER);
        container.getStylesheets().addAll("/ui/css/aim.css");
        container.getStyleClass().addAll("popup-container");

        popup.getContent().add(container);
        return popup;
    }

    public void showPopupMessage(final String message, final Region node, final double width, final double height) {
        final Popup popup = createPopup(message);
        imagesGridView.setOnMouseReleased(event -> popup.hide());
        popup.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                popup.setX(node.getScene().getWindow().getX() + node.getWidth() / 2 - popup.getWidth() / 2);
                popup.setY(node.getScene().getWindow().getY() + node.getHeight() / 2 - popup.getHeight() / 2);
            }
        });
        popup.show(node, width, height);
    }

    private static class ImageViewCell extends GridCell<ImageItem> {
        private Label dpiSize;
        private Label dpiType;
        private ImageView imageView;
        private StackPane imageViewContainer;
        private ToggleButton toggleButton;
        private VBox container;

        private ImageViewCell() {
            this.container = new VBox();
            this.dpiSize = new Label();
            this.dpiType = new Label();
            this.imageView = new ImageView();
            this.imageViewContainer = new StackPane(imageView);
            this.toggleButton = new ToggleButton("", container);

            UiUtils.setSize(this, 100, 100);
            UiUtils.setSize(imageView, 60, 60);
            UiUtils.setSize(toggleButton, 100, 100);

            container.setAlignment(Pos.CENTER);
            container.getChildren().addAll(imageViewContainer, dpiType, dpiSize);
            imageViewContainer.getStyleClass().addAll("aim-image-view-container");

            setGraphic(toggleButton);
        }

        @Override
        protected void updateItem(final ImageItem item, final boolean empty) {
            final ImageItem oldItem = getItem();
            if (oldItem != null) {
                toggleButton.selectedProperty().unbindBidirectional(oldItem.isSelectedProperty());
            }

            super.updateItem(item, empty);

            if (empty || item == null) {
                dpiSize.setText("");
                dpiType.setText("");
                imageView.setImage(null);
                toggleButton.setOnAction(null);
                toggleButton.setTooltip(null);
            } else {
                try {
                    dpiSize.setText(getDpiSize(item));
                    dpiType.setText(getDpiType(item));
                    imageView.setImage(new Image(item.getFile().toURI().toURL().toExternalForm()));
                    toggleButton.setOnAction(event -> item.isSelectedProperty().setValue(toggleButton.isSelected()));
                    toggleButton.selectedProperty().bindBidirectional(item.isSelectedProperty());
                    toggleButton.setTooltip(new Tooltip(item.getFile().getName()));
                } catch (final MalformedURLException e) {}
            }
        }

        private String getDpiSize(final ImageItem imageItem) {
            final String fileName = imageItem.getFile().getName();

            if (fileName.endsWith("18dp.png")) {
                return "18dp";
            } else if (fileName.endsWith("24dp.png")) {
                return "24dp";
            } else if (fileName.endsWith("36dp.png")) {
                return "36dp";
            } else if (fileName.endsWith("48dp.png")) {
                return "48dp";
            } else {
                return "";
            }
        }

        private String getDpiType(final ImageItem imageItem) {
            final String parentFileName = imageItem.getFile().getParentFile().getName();

            if (parentFileName.contains("xxxhdpi")) {
                return "xxxhdpi";
            } else if (parentFileName.contains("xxhdpi")) {
                return "xxhdpi";
            } else if (parentFileName.contains("xhdpi")) {
                return "xhdpi";
            } else if (parentFileName.contains("hdpi")) {
                return "hdpi";
            } else if (parentFileName.contains("mdpi")) {
                return "mdpi";
            } else if (parentFileName.contains("ldpi")) {
                return "ldpi";
            } else {
                return "";
            }
        }
    }

    // FIXME Without skin gridView will not be updated after choosing "mdpi" to "hdpi" in cbAllowedDpiType
    private static class MyGridViewSkin extends GridViewSkin<ImageItem> {
        MyGridViewSkin(GridView<ImageItem> control) {
            super(control);
        }

        public void redrawCells() {
            super.flow.recreateCells();
        }
    }
}
