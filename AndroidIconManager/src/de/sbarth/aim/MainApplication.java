package de.sbarth.aim;

import java.net.URL;

import de.sbarth.aim.ui.IconOrganisatorView;
import de.sbarth.aim.viewmodel.IconOrganisatorViewModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApplication extends Application {
    public static final long MAX_AVAILABLE_IMAGES = 68956;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final URL location = getClass().getResource("/ui/layout/icon-organisator-view.fxml");
        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);

        final Parent view = fxmlLoader.load(location.openStream());
        final IconOrganisatorView codeBehindView = fxmlLoader.getController();
        final IconOrganisatorViewModel viewModel = new IconOrganisatorViewModel();
        codeBehindView.setViewModel(viewModel);

        viewModel.loadImagesAsync("./");
//        viewModel.loadImagesAsync("C:\\Users\\x1sba\\Downloads\\material-design-icons-master");

        final Scene scene = new Scene(view);
//        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
