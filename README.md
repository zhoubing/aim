# Android Icon Manager #
## Welcome tired Android developer ##

Are you tired of searching images for your Android app and copying each image file into its own drawable folder? I had the same problem and each icon I have copied I thought that I am wasting my time. Especially when Google published their big "Material Design Icon" pack. Nearly 70,000 icons in too many folders. So I thought that I need a tool which reads all icons of a folder and give me the chance to copy them easily into my project drawable folder. And here it is :-)

### Set up ###
* I had some trouble to display too much icons, so be sure that you have at least **JDK 1.8.40** installed
* You need a folder with PNG files which were **organized in drawable-[dpi-type] folders** (I would recommend to use the Material Design Icon pack from Google [available here](https://github.com/google/material-design-icons))
* The you copy the following [Jar](https://bitbucket.org/sistbart/aim/src/32649231e7715e51c6f6e8b8efad3352dab9c1cd/Release/AndroidIconManager.jar?at=master) into the root folder of the icon set (e.g. C:\users\my-name\material-design-icons\AndroidIconManager.jar) - **For newer versions, see [here](https://bitbucket.org/sistbart/aim/src/976832a666c18f49dd20689d816dfc93b18dc966/Release/?at=master)**
* Start the Jar and you should see following screen after a loading of some seconds (70,000 icons needs some time):

![Android Icon Manager Startscreen.png](https://bitbucket.org/repo/y9Byna/images/2247898387-Android%20Icon%20Manager%20Startscreen.png)

And thats it :-)

### How do I copy some images into my project folder? ###

* Choose your project path and click on "Open Directory Chooser":

![Android Icon Manager Directory Chooser.png](https://bitbucket.org/repo/y9Byna/images/504093107-Android%20Icon%20Manager%20Directory%20Chooser.png)

* Search for some words (*I only check if the search criterias were inside the file path of the image*) Google made a lot effort to give meaningful names to their images. If you want to search for more than one criteria, you have to split each word with ';' (e.g. 'black;wallet;18dp' returns black images with a wallet as symbol and a size of 18dp's): 

![Android Icon Manager Search.png](https://bitbucket.org/repo/y9Byna/images/3038823033-Android%20Icon%20Manager%20Search.png)

* Click on each icon you would like to import to your project folder, I would recommend that you also activate the checkbox 'Only Android Icons', because these icons were made for Android ;-) Below each icon is also the information which dpi size and type the icon itself has. If you like an image just click on it and you should see it toggled:

![Android Icon Manager Selected Images.png](https://bitbucket.org/repo/y9Byna/images/1286720898-Android%20Icon%20Manager%20Selected%20Images.png)

* click on 'Copy images to destination folder' on the right side of 'Open Directory Chooser': 

![Android Icon Manager Copy Images.png](https://bitbucket.org/repo/y9Byna/images/430207715-Android%20Icon%20Manager%20Copy%20Images.png)

* All icons were copied into its dpi dependent folder, e.g. icons with dpi type 'xhdpi' were copied into 'drawable-xhdpi' and icons without this information will be copied into 'drawable' (I only check if the name of the parent folder ends with xhdpi or another dpi type, the icons from Google were organized by this and that is the point why I recommend to use this tool with the Google Material Icon set). When all images were copied successfully into the project a folder, a notification will be displayed.

That's all for today, I hope you can need it. If you need more functions or you want to have a better style than the default one, you can checkout the whole project and just add your needs. You only need some JavaFX knowledge :-)  

Have fun!